# Starfinder Statblock Parser

A simple FoundryVTT module that allows GMs to parse the statblocks of Starfinder NPCs so they can quickly create actors.

To use simply go to the actors tab in FoundryVTT, and click the Parse Statblock button at the bottom. Paste in the statblock text, and click ok.